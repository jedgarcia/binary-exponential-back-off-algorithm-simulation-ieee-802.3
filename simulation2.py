import simpy
import random
import math
import sys

#Functions and Classes==========================================
class Host:

    #Variables
    def __init__(self):
        global arrivalRate
        self.nextTimeToSend = round(random.expovariate(sendingRate))
        self.lastAttempt = 0
        self.colissionInARow = 0
        self.queue = 0
        self.arrivalRate = arrivalRate
        self.nextArrivalTime  = round(random.expovariate(arrivalRate))
        self.lastArrival = 0
        inBackOff = False 

    #This checks if we have an incoming packet, if we do it
    #calculates the next time were supposed to get another one
    def doWeHaveAPacket(self, time):
        firstTime = False
        if(self.queue == 0):
            firstTime = True

        if (time - self.lastArrival) >= self.nextArrivalTime :
            self.queue += 1
            self.nextArrivalTime = round(random.expovariate(arrivalRate))
            self.lastArrival = time
        if firstTime:
            self.calcNextNormalTime()

    def calcNextNormalTime(self):
        self.nextTimeToSend = round(random.expovariate(sendingRate))


    def calcBinaryBackOffTime(self):
        n = self.colissionInARow
        self.nextTimeToSend = self.binaryBackOffTime(n)


    def binaryBackOffTime(self, n):
        k = min(n, 1024)
        return random.randint(0, math.pow(2, k) )


    def getNextTime(self):
        return self.nextTimeToSend


    def getLastSuccesTime(self):
        return self.getLastSuccesTime


    def sucessfulSend(self, time):
        global numOfSuccess
        if self.queue != 0 :
            numOfSuccess +=1
            self.inBackOff = False
            self.lastAttempt = time
            self.calcNextNormalTime()
            self.colissionInARow = 0
            self.queue -= 1


    #will put the host into backoff mode, the host will now
    #turn on BackOff
    #Add to the colissionInArow
    #calculate the new backofftime
    def collission(self, time):
        global numOfCollision
        numOfCollision+=1

        self.inBackOff = True
        self.colissionInARow+=1
        self.calcBinaryBackOffTime()
        self.lastAttempt = time


    def isReadyToSend(self, time):
        if (time - self.lastAttempt) >= self.nextTimeToSend and self.queue > 0 :
            return True
        else:
            return False



#==============================================================
def whatIsThisRound(time):
    global hostArray
    readyToSend = False

    listOfSendingHosts = []

    #going through the hosts to see what type of round this is
    for i in range(numberOfHosts):
        #This checks if we should have a packet in the queue
        hostArray[i].doWeHaveAPacket(time)

        #This then checks if they wants to send
        if hostArray[i].isReadyToSend(time):
            listOfSendingHosts.append(i)

    return listOfSendingHosts
#==============================================================

def simulate(env):
    global sucessfullySent
    global numberOfHosts
    global ourTime

    while True:

        #What type of party is this
        arrayOfIndex = whatIsThisRound(env.now)

        if len(arrayOfIndex) > 1 :
            #start collissions of the ones that tried
            #print 'There was a collision'
            for i in arrayOfIndex:
                hostArray[i].collission(env.now)
            ourTime+=1

        
        elif len(arrayOfIndex) == 1:
            #print 'Success'
            #success
            hostArray[0].sucessfulSend(env.now)
            ourTime +=1

        #Setting up the intervals 
        time_interval = 1
        yield env.timeout(time_interval)



#Global Variables===============================================
arrivalRate = float(sys.argv[1])
numberOfHosts = int(sys.argv[2])
sendingRate = 1
numOfCollision = 0
numOfSuccess = 0
maxTime = 15000

#Throughput does not include the time that no nodes tried to send anything
ourTime = 0

#Declaring a list that contains a variable amount of
hostArray = []
for i in range(numberOfHosts):
    hostArray.append(Host())

#Main Program===================================================

env = simpy.Environment()
env.process(simulate(env))
env.run(until=maxTime)

"""print 'number of successes: %d' % numOfSuccess
print 'number of collissions: %d' % numOfCollision
print 'maxTime: %d' % maxTime"""

print 'Throughput for lambda:%1.2lf, N: %d : %1.4lf' % (arrivalRate, numberOfHosts,numOfSuccess*1.00/(numOfSuccess+numOfCollision)*1.00)
